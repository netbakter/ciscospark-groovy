// first, get the HTTPBuilder libraries and dependencies (using the Groovy Grapes system):
@GrabConfig(systemClassLoader=true)
@Grapes([
    @Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.2'),
    @Grab(group='net.sf.json-lib', module='json-lib', version='2.4', classifier='jdk15'),
    @Grab(group='org.apache.httpcomponents', module='httpclient', version='4.4'),
    @Grab(group='xml-resolver', module='xml-resolver', version='1.2')
])
import groovyx.net.http.*                                                                // importing the Groovy HTTPBuilder classes
def sparkUrl =       "https://api.ciscospark.com/v1/"                                    // Spark base URL
def apiToken =       "YThmZWZiMTgtODE3MC00ZDFkLWI2YTktM2NlMTA3MjQ4Y2JhNDY1NTIyMmQtOTIy"  // this is my access token, get yours on https://developer.ciscospark.com/getting-started.html
def http = new HTTPBuilder(sparkUrl)                                                     // creating a new HTTPBuilder object with the Spark base URL
def roomId = ""                                                                          // room ID, returned when room is created
def roomTitle = "Greg's ne wroom!"                                                       // room title
def goodRoomTitle = "Greg's new room!"                                                   // actually, this is the correct room title!
int sleepTime = 10000                                                                    // sleep this amount of milliseconds between operations


println("==================================")
println("create a new room - C")
http.request(Method.POST, ContentType.JSON) { request ->                        
    uri.path                 = 'rooms'
    headers."Authorization"  = 'Bearer ' + apiToken
    body                     = [title:roomTitle]
    response.success = {respMetadata, returnedContent ->
        println respMetadata?.dump()
        println returnedContent?.dump()
        roomId = returnedContent.id
    }
    response.failure = {respMetadata, returnedContent -> 
        println respMetadata?.dump()
        println returnedContent?.dump()
    }
}
println "New room id: ${roomId}"

// wait:
sleep(sleepTime)

/* rename the room: U */
println("==================================")
println("rename the room - U")
http.request(Method.PUT, ContentType.JSON) { request ->                        
    uri.path                 = 'rooms' + '/' + roomId
    headers."Authorization"  = 'Bearer ' + apiToken
    body                     = [title:goodRoomTitle]
    response.success = {respMetadata, returnedContent ->
        println respMetadata?.dump()
        println returnedContent?.dump()
        roomId = returnedContent.id
    }
    response.failure = {respMetadata, returnedContent -> 
        println respMetadata?.dump()
        println returnedContent?.dump()
    }
}

// wait:
sleep(sleepTime)

println("==================================")
println("get the room details - R")
http.request(Method.GET, ContentType.JSON) { request ->                        
    uri.path                 = 'rooms' + '/' + roomId
    headers."Authorization"  = 'Bearer ' + apiToken
    response.success = {respMetadata, returnedContent ->
        println respMetadata?.dump()
        println returnedContent?.dump()
    }
    response.failure = {respMetadata, returnedContent -> 
        println respMetadata?.dump()
        println returnedContent?.dump()
    }
}

// wait:
sleep(sleepTime)

/* delete the room: D */
println("==================================")
println("delete the room - D")
http.request(Method.DELETE, ContentType.JSON) { request ->
    uri.path                 = 'rooms' +'/' + roomId
    headers."Authorization"  = 'Bearer ' + apiToken
    response.success = {respMetadata, returnedContent ->
        println respMetadata?.dump()
        println returnedContent?.dump()
    }
    response.failure = {respMetadata, returnedContent -> 
        println respMetadata?.dump()
        println returnedContent?.dump()
    }
}



return null