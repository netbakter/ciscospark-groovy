// first, get the HTTPBuilder libraries and dependencies (using the Groovy Grapes system):
@GrabConfig(systemClassLoader=true)
@Grapes([
    @Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.2'),
    @Grab(group='net.sf.json-lib', module='json-lib', version='2.4', classifier='jdk15'),
    @Grab(group='org.apache.httpcomponents', module='httpclient', version='4.4'),
    @Grab(group='xml-resolver', module='xml-resolver', version='1.2')
])
import groovyx.net.http.*                                                                // importing the Groovy HTTPBuilder classes
def sparkUrl =       "https://api.ciscospark.com/v1/"                                    // Spark base URL
def apiToken =       "YThmZWZiMTgtODE3MC00ZDFkLWI2YTktM2NlMTA3MjQ4Y2JhNDY1NTIyMmQtOTIy"  // this is my access token, get yours on https://developer.ciscospark.com/getting-started.html
def http = new HTTPBuilder(sparkUrl)                                                     // creating a new HTTPBuilder object with the Spark base URL

/* listing all my rooms */
http.request(Method.GET, ContentType.JSON) { request ->                        // invoking a HTTP GET Request, no params
    uri.path                 = 'rooms'                                                   // path is 'rooms', for other URI's see https://developer.ciscospark.com/quick-reference.html
    headers."Authorization"  = 'Bearer ' + apiToken                                      // this is how we tell Spark who we are (and whether we are authenticated)
    response.success = {response, returnedContent ->                                     // callback to handle successful invokation, giving us metadata (response) and the content (returnedContent)
        assert response.status == 200                                                    // asserting we are getting HTTP 200 response (OK), would fail if something not quite right
        returnedContent.items.each { item ->                                                  // iterating over the objects within returnedContent
            printf("%40s lastActivity:%22s id:$item.id%n", [item.title, item.lastActivity])   // neat print of room title, lastActivity and id
        }
    }
}

println("==================================")
/* listing all people with displayName starting Sparky */
http.request(Method.GET, ContentType.JSON) { request ->                                   
    uri.path                 = 'people'                                                  // path is 'people', for other URI's see https://developer.ciscospark.com/quick-reference.html
    uri.query                = [displayName:"Gergely"]                                    // add the following GET param to the query: displayName=Gergely
    headers."Authorization"  = 'Bearer ' + apiToken                                      // this is how we tell Spark who we are (and whether we are authenticated)
    response.success = {response, returnedContent ->                                     // callback to handle successful invokation, giving us metadata (response) and the content (returnedContent)
        assert response.status == 200                                                    // asserting we are getting HTTP 200 response (OK), would fail if something not quite right
        returnedContent.items.each { item ->                                                  // iterating over the objects within returnedContent
            printf("%40s %s id:$item.id%n", [item.displayName, item.emails])               // neat print of persons
        }
    }
    response.failure = {response, reader ->
        println response.dump()
        println reader.dump()
    }
}

println("==================================")
/* writing to the room with title: Sparky, to a person named Sparky */
http.request(Method.POST) { request ->                        // notice we are using HTTP POST here
    uri.path                 = "messages"
    headers."Authorization"  = 'Bearer ' + apiToken
    requestContentType       = ContentType.JSON
    body                     = [text:"Hi Sparky!",toPersonId:"722bb271-d7ca-4bce-a9e3-471e4412fa77"]
    response.success = {response, returnedContent ->                                     
        println response.dump()
        println returnedContent.dump()
    }
}
println("==================================")
/* writing to the room with title: Krakow CX Team, this is room id Y2lzY29zcGFyazovL3VzL1JPT00vYzQyZmE3YzAtZTQzMy0xMWU0LTk4NmMtOTlkMjk5ZTQ0NWY4 */

http.request(Method.POST) { request ->                       
    uri.path                 = "messages"
    headers."Authorization"  = 'Bearer ' + apiToken
    requestContentType       = ContentType.JSON
    body                     = [text:"[API] Testing i18n with UTF8. PT: O cão tem um coração à Brás\npropósito\nibérica\nalô tricô\n", roomId: "Y2lzY29zcGFyazovL3VzL1JPT00vYzQyZmE3YzAtZTQzMy0xMWU0LTk4NmMtOTlkMjk5ZTQ0NWY4"]
    println request.dump()
    response.success = {response, returnedContent ->                                     
        println response.dump()
        println returnedContent.dump()
    }
        response.failure = {response, reader ->
        println response.dump()
        println reader.dump()
    }
}

return null